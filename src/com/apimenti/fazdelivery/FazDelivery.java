package com.apimenti.fazdelivery;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;


import android.widget.EditText;
import android.widget.TextView;

import com.apimenti.fazdelivery.post.Post;

import org.json.*;

public class FazDelivery extends Activity implements OnClickListener {

	private Button button;
    private EditText editText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_faz_delivery);

		button = (Button) findViewById(R.id.button1);
		editText = (EditText) findViewById(R.id.editText1);
		button.setOnClickListener(this);

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                boolean handled = false;
                if (i == EditorInfo.IME_ACTION_SEARCH || i == EditorInfo.IME_ACTION_SEND) {
                    button.performClick();
                    handled = true;
                }
                return handled;
            }
        });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.faz_delivery, menu);
		return true;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void onClick(View v) {

        String text = editText.getText().toString();

//            Geocoder geocoder;
//            geocoder = new Geocoder(this, Locale.getDefault());

//            GPSTracker gps = new GPSTracker(this);

//            List<Address> addresses;

//            if (gps.canGetLocation()) {
//                lat = gps.getLatitude();
//                lng = gps.getLongitude();


//                addresses = geocoder.getFromLocation(lat,lng,1);

//                neighborhood = addresses.get(0).getSubLocality();

//            }
//            else {
//                gps.showSettingsAlert();
//            }


        Intent intent = new Intent(FazDelivery.this, Result.class);
        intent.putExtra("text", text);

        startActivity(intent);

    }

}
