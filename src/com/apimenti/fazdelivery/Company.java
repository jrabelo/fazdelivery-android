package com.apimenti.fazdelivery;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.apimenti.fazdelivery.adapter.ProductAdapter;
import com.apimenti.fazdelivery.adapter.ProductPriceAdapter;
import com.apimenti.fazdelivery.post.CompanyPost;
import com.apimenti.fazdelivery.post.CompanyProductsPost;
import com.apimenti.fazdelivery.post.Designer;
import com.apimenti.fazdelivery.post.ProductPost;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by joaopaulo on 20/05/13.
 */
public class Company extends Activity implements AdapterView.OnItemClickListener {

    TabHost tabHost;
    JSONObject lp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);

        Bitmap logo = getIntent().getParcelableExtra("logo");
        String cid = getIntent().getStringExtra("id");
        String pid = getIntent().getStringExtra("pid");
        String a;

        try {
            CompanyPost cp = new CompanyPost();
            cp.execute(cid);
            a = cp.get();

            JSONObject json = new JSONObject(a);

            String addrname = json.getJSONObject("Address").getString("name");
            String addrnmbr = json.getJSONObject("Address").getString("number");
            String addrngbh = json.getJSONObject("Address").getString("neighborhood");
            String addrrefs = json.getJSONObject("Address").getString("reference");
            String addrcity = json.getJSONObject("Address").getString("city");
            String addrstat = json.getJSONObject("Address").getString("state");

            String telnumbers = null;
            for (int i = 0; i < json.getJSONArray("Telephone").length();i++) {
                String num = json.getJSONArray("Telephone").getJSONObject(i).getString("phone_number");
                String number = null;
                if (num.length() == 10) {
                    number = "("+ num.substring(0,2) + ") " + num.substring(2,6) + "-" + num.substring(6,10);
                }
                else {
                    number = num;
                }
                if (i == 0) {
                    telnumbers = number;
                }
                else {
                    telnumbers += "\n"+number;
                }
            }

            JSONObject hours = new JSONObject(json.getJSONObject("Company").getString("business_hours"));

            TextView monOpen = (TextView) findViewById(R.id.monOpen);
            TextView monClose = (TextView) findViewById(R.id.monClose);
            TextView tueOpen = (TextView) findViewById(R.id.tueOpen);
            TextView tueClose = (TextView) findViewById(R.id.tueClose);
            TextView wedOpen = (TextView) findViewById(R.id.wedOpen);
            TextView wedClose = (TextView) findViewById(R.id.wedClose);
            TextView thuOpen = (TextView) findViewById(R.id.thuOpen);
            TextView thuClose = (TextView) findViewById(R.id.thuClose);
            TextView friOpen = (TextView) findViewById(R.id.friOpen);
            TextView friClose = (TextView) findViewById(R.id.friClose);
            TextView satOpen = (TextView) findViewById(R.id.satOpen);
            TextView satClose = (TextView) findViewById(R.id.satClose);
            TextView sunOpen = (TextView) findViewById(R.id.sunOpen);
            TextView sunClose = (TextView) findViewById(R.id.sunClose);

            if (hours.has("MON-hour-open-1")) {
                monOpen.setText(hours.getString("MON-hour-open-1")+":"+hours.getString("MON-minute-open-1"));
                monClose.setText(hours.getString("MON-hour-close-1")+":"+hours.getString("MON-minute-close-1"));
            }if (hours.has("TUE-hour-open-1")) {
                tueOpen.setText(hours.getString("TUE-hour-open-1")+":"+hours.getString("TUE-minute-open-1"));
                tueClose.setText(hours.getString("TUE-hour-close-1")+":"+hours.getString("TUE-minute-close-1"));
            }if (hours.has("WED-hour-open-1")) {
                wedOpen.setText(hours.getString("WED-hour-open-1")+":"+hours.getString("WED-minute-open-1"));
                wedClose.setText(hours.getString("WED-hour-close-1")+":"+hours.getString("WED-minute-close-1"));
            }if (hours.has("THU-hour-open-1")) {
                thuOpen.setText(hours.getString("THU-hour-open-1")+":"+hours.getString("THU-minute-open-1"));
                thuClose.setText(hours.getString("THU-hour-close-1")+":"+hours.getString("THU-minute-close-1"));
            }if (hours.has("FRI-hour-open-1")) {
                friOpen.setText(hours.getString("FRI-hour-open-1")+":"+hours.getString("FRI-minute-open-1"));
                friClose.setText(hours.getString("FRI-hour-close-1")+":"+hours.getString("FRI-minute-close-1"));
            }if (hours.has("SAT-hour-open-1")) {
                satOpen.setText(hours.getString("SAT-hour-open-1")+":"+hours.getString("SAT-minute-open-1"));
                satClose.setText(hours.getString("SAT-hour-close-1")+":"+hours.getString("SAT-minute-close-1"));
            }if (hours.has("SUN-hour-open-1")) {
                sunOpen.setText(hours.getString("SUN-hour-open-1")+":"+hours.getString("SUN-minute-open-1"));
                sunClose.setText(hours.getString("SUN-hour-close-1")+":"+hours.getString("SUN-minute-close-1"));
            }


            tabHost=(TabHost)findViewById(R.id.tabHost);
            tabHost.setup();

            TabSpec spec1=tabHost.newTabSpec("SOBRE");
            spec1.setContent(R.id.tab1);
            spec1.setIndicator("SOBRE");

            TextView t = (TextView) findViewById(R.id.companyName);
            TextView tel = (TextView) findViewById(R.id.companyTelephone);
            TextView addr = (TextView) findViewById(R.id.companyAddress);
            ImageView cLogo = (ImageView) findViewById(R.id.tabCompanyImage);

            cLogo.setImageBitmap(logo);

            LinearLayout paymentArea = (LinearLayout) findViewById(R.id.payment);

            for (int i = 0; i < json.getJSONArray("AcceptedPayment").length(); i++) {
                LinearLayout base = new LinearLayout(getApplicationContext());

                base.setLayoutParams(new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                ));

                base.setOrientation(LinearLayout.HORIZONTAL);

                TextView paymentName = new TextView(getApplicationContext());
                paymentName.setText(json.getJSONArray("AcceptedPayment").getJSONObject(i).getString("name"));
                paymentName.setLayoutParams(new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                ));
                paymentName.setTextColor(Color.BLACK);

                paymentName.setPadding(10,0,0,0);

                ImageView icon = new ImageView(getApplicationContext());
                icon.setPadding(5,0,0,0);

                List<String> ic = new ArrayList<String>();

                ic.add("http://fazdelivery.com.br/img/"+json.getJSONArray("AcceptedPayment").getJSONObject(i).getString("icon"));

                Designer d = new Designer();
                d.execute(ic);

                List<Bitmap> draw = d.get();

                icon.setImageBitmap(draw.get(0));

                base.addView(icon);

                base.addView(paymentName);
                paymentArea.addView(base);

            }

            t.setText(json.getJSONObject("Company").getString("name"));
            tel.setText(telnumbers);
            addr.setText(addrname+" "+addrnmbr+", "+addrngbh+"\n"+addrcity+" - "+addrstat+"\n"+addrrefs);

            TabSpec spec2=tabHost.newTabSpec("PRODUTOS");
            spec2.setIndicator("PRODUTOS");
            spec2.setContent(R.id.tab2);

            CompanyProductsPost cpp = new CompanyProductsPost();
            cpp.execute(cid);
            lp = new JSONObject(cpp.get());

            lp.remove("validation_errors");

            ProductAdapter padapter = new ProductAdapter(getApplicationContext(),lp);

            ListView listProducts = (ListView) findViewById(R.id.companyProductList);

            listProducts.setOnItemClickListener(this);

            listProducts.setAdapter(padapter);

            TabSpec spec3=tabHost.newTabSpec("DETALHES");
            spec3.setContent(R.id.tab3);
            spec3.setIndicator("DETALHES");

            ProductPost pp = new ProductPost();

            pp.execute(pid);

            JSONObject j = new JSONObject(pp.get());

            TextView title = (TextView) findViewById(R.id.productDetailTitle);
            title.setText(j.getJSONObject("Product").getString("name"));

            TextView desc = (TextView) findViewById(R.id.productDetailDescription);
            desc.setText(j.getJSONObject("Product").getString("description"));

            ImageView productImage = (ImageView) findViewById(R.id.img_details);
            List<String> pi = new ArrayList<String>();
            pi.add(j.getJSONObject("Product").getString("image"));
            Designer designer = new Designer();
            designer.execute(pi);
            List<Bitmap> drawProduct = designer.get();
            productImage.setImageBitmap(drawProduct.get(0));

            ListView list = (ListView) findViewById(R.id.pricelist);

            ProductPriceAdapter adapter = new ProductPriceAdapter(getApplicationContext(),j.getJSONArray("ProductVariation"));
            list.setAdapter(adapter);

            tabHost.addTab(spec1);
            tabHost.addTab(spec2);
            tabHost.addTab(spec3);

            tabHost.setCurrentTab(2);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        ProductPost pp = new ProductPost();
        JSONArray keys = lp.names();
        try {
            pp.execute(lp.getJSONObject(keys.getString(i)).getString("id"));
            JSONObject j = new JSONObject(pp.get());

            TextView title = (TextView) findViewById(R.id.productDetailTitle);
            title.setText(j.getJSONObject("Product").getString("name"));

            TextView desc = (TextView) findViewById(R.id.productDetailDescription);
            desc.setText(j.getJSONObject("Product").getString("description"));

            ImageView productImage = (ImageView) findViewById(R.id.img_details);
            List<String> pi = new ArrayList<String>();
            pi.add(j.getJSONObject("Product").getString("image"));
            Designer designer = new Designer();
            designer.execute(pi);
            List<Bitmap> drawProduct = designer.get();
            productImage.setImageBitmap(drawProduct.get(0));

            ListView list = (ListView) findViewById(R.id.pricelist);

            ProductPriceAdapter adapter = new ProductPriceAdapter(getApplicationContext(),j.getJSONArray("ProductVariation"));
            list.setAdapter(adapter);

            tabHost.setCurrentTab(2);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
