package com.apimenti.fazdelivery.post;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by joaopaulo on 29/05/13.
 */
public class ProductPost extends AsyncTask<String,String,String> {

    public ProductPost(){}

    public String search(String pid) {
        String url = "http://dev.fazdelivery.com.br/mobile/products_r_e_s_t/"+pid+".json";

        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);
        String responseBody = null;

        try {
            HttpResponse response = httpclient.execute(httpget);
            responseBody = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseBody;
    }

    @Override
    protected String doInBackground(String... params) {
        String response = search(params[0]);
        return response;
    }
}
