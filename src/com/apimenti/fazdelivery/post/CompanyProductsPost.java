package com.apimenti.fazdelivery.post;

import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

/**
 * Created by joaopaulo on 29/05/13.
 */
public class CompanyProductsPost extends AsyncTask<String,String,String> {

    public CompanyProductsPost(){}

    public String search(String cid) {
        String url = "http://dev.fazdelivery.com.br/mobile/company_products_r_e_s_t/"+cid+".json";

        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);
        String responseBody = null;

        try {
            HttpResponse response = httpclient.execute(httpget);
            responseBody = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseBody;
    }

    @Override
    protected String doInBackground(String... params) {
        String response = search(params[0]);
        return response;
    }
}
