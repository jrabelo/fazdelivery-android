package com.apimenti.fazdelivery.post;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;

import com.apimenti.fazdelivery.FazDelivery;
import com.apimenti.normalizer.Normalizer;

public class Post extends AsyncTask<String, String, String> {

    ProgressDialog dialog;
    Activity mActivity;

    public Post() {
    }
    public Post(Activity mActivity) {
        this.mActivity = mActivity;
    }


    public String search(String text, String neighborhood, String lat, String lng, String from, String to) {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://dev.fazdelivery.com.br/mobile/search_r_e_s_t/1.json");
		String responseBody = null;

        if (neighborhood.compareTo("Caminho das Árvore") == 0) {
            neighborhood = "Caminho das Árvores";
        }

        neighborhood = Normalizer.normalize(neighborhood, Normalizer.Form.NFD);
        neighborhood = neighborhood.toLowerCase().replaceAll("[^\\p{ASCII}]","");

        try {

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
//			nameValuePairs.add(new BasicNameValuePair("page", String.valueOf(page)));
            nameValuePairs.add(new BasicNameValuePair("from", from));
            nameValuePairs.add(new BasicNameValuePair("to", to));
			nameValuePairs.add(new BasicNameValuePair("search_type", "1"));
            nameValuePairs.add(new BasicNameValuePair("text", text));
            nameValuePairs.add(new BasicNameValuePair("neighborhood", neighborhood));
            nameValuePairs.add(new BasicNameValuePair("latitude", lat));
            nameValuePairs.add(new BasicNameValuePair("longitude", lng));
            nameValuePairs.add(new BasicNameValuePair("distance_preference", "0.5"));
            nameValuePairs.add(new BasicNameValuePair("price_preference", "0.5"));
            nameValuePairs.add(new BasicNameValuePair("delivery_time_preference", "0.5"));
            nameValuePairs.add(new BasicNameValuePair("segment_id", "-1"));
            nameValuePairs.add(new BasicNameValuePair("provider_id", "-1"));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);

			responseBody = EntityUtils.toString(response.getEntity());

            Log.d("post",responseBody);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseBody;
	}

    @Override
    protected void onPreExecute() {
//        dialog = ProgressDialog.show(mActivity, "","Loading. Please wait...", true);
    }

    @Override
    protected void onPostExecute(String s) {
//        dialog.dismiss();
    }

    @Override
	protected String doInBackground(String... params) {
        String text = params[0];
        String neighborhood = params[1];
        String lat = params[2];
        String lng = params[3];
//        String page = params[4];
        String from = params[4];
        String to = params[5];
        return search(text,neighborhood,lat,lng,from,to);
	}

}
