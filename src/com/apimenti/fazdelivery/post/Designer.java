package com.apimenti.fazdelivery.post;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by joaopaulo on 21/05/13.
 */
public class Designer extends AsyncTask<List<String>,Void,List<Bitmap>> {

    public Bitmap loadImage(String url) {
        try {
            Bitmap d;
            InputStream is = (InputStream) new URL(url).getContent();
            d = BitmapFactory.decodeStream(is);
            return d;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected List<Bitmap> doInBackground(List<String>... strings) {

        HashMap<String,Bitmap> base = new HashMap<String,Bitmap>();
        List<Bitmap> d = new ArrayList<Bitmap>();
        for(String s: strings[0]) {
            if (!base.containsKey(s)) {
                base.put(s,loadImage(s));
            }
            d.add(base.get(s));
        }
        return d;
    }
}
