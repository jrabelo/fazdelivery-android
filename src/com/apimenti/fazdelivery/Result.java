package com.apimenti.fazdelivery;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.apimenti.fazdelivery.adapter.SearchAdapter;
import com.apimenti.fazdelivery.post.Designer;
import com.apimenti.normalizer.Normalizer;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Result extends ListActivity implements OnItemClickListener {

    private ArrayList<Products> l;
    List<Bitmap> bitmaps;

    public Result() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        final List<String> urls = new ArrayList<String>();
        String neighborhood;
        double lat;
        double lng;

        l = new ArrayList<Products>();
        lat = -11.32343;
        lng = -38.12321;
        neighborhood = "pituba";

        class Post extends AsyncTask<String, String, String> {

            ProgressDialog dialog;
            Activity mActivity;

            public Post(Activity mActivity) {
                this.mActivity = mActivity;
            }


            public String search(String text, String neighborhood, String lat, String lng, String from, String to) {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(
                        "http://dev.fazdelivery.com.br/mobile/search_r_e_s_t/1.json");
                String responseBody = null;

                if (neighborhood.compareTo("Caminho das Árvore") == 0) {
                    neighborhood = "Caminho das Árvores";
                }

                neighborhood = Normalizer.normalize(neighborhood, Normalizer.Form.NFD);
                neighborhood = neighborhood.toLowerCase().replaceAll("[^\\p{ASCII}]","");

                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                    nameValuePairs.add(new BasicNameValuePair("from", from));
                    nameValuePairs.add(new BasicNameValuePair("to", to));
                    nameValuePairs.add(new BasicNameValuePair("search_type", "1"));
                    nameValuePairs.add(new BasicNameValuePair("text", text));
                    nameValuePairs.add(new BasicNameValuePair("neighborhood", neighborhood));
                    nameValuePairs.add(new BasicNameValuePair("latitude", lat));
                    nameValuePairs.add(new BasicNameValuePair("longitude", lng));
                    nameValuePairs.add(new BasicNameValuePair("distance_preference", "0.5"));
                    nameValuePairs.add(new BasicNameValuePair("price_preference", "0.5"));
                    nameValuePairs.add(new BasicNameValuePair("delivery_time_preference", "0.5"));
                    nameValuePairs.add(new BasicNameValuePair("segment_id", "-1"));
                    nameValuePairs.add(new BasicNameValuePair("provider_id", "-1"));
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpclient.execute(httppost);

                    responseBody = EntityUtils.toString(response.getEntity());

                    Log.d("post",responseBody);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return responseBody;
            }

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(mActivity, "","Carregando...", true);
            }

            @Override
            protected void onPostExecute(String s) {
                try {
                    JSONObject j = new JSONObject(s);
                    JSONArray json = j.getJSONArray("items");


                    for (int i = 0; i< json.length(); i++) {

                        Products aux = new Products();

                        JSONObject p = json.getJSONObject(i);

                        aux.setName(p.getJSONObject("Product").getString("name"));
                        aux.setPrice(p.getJSONArray("ProductVariation").getJSONObject(0).getString("price"));
                        aux.setCompany_name(p.getJSONObject("Company").getJSONObject("Company").getString("name"));
                        aux.setCompany_id(p.getJSONObject("Company").getJSONObject("Company").getString("id"));
                        aux.setProductId(p.getJSONObject("Product").getString("id"));
                        l.add(aux);
                        String url = null;

                        url = p.getJSONObject("Company").getJSONObject("Company").getString("image_url");

                        urls.add(url);
                    }

                    ListView list = getListView();

                    Designer drawing = new Designer();
                    drawing.execute(urls);
                    bitmaps = drawing.get();

                    SearchAdapter adapter = new SearchAdapter(getApplicationContext(),l,bitmaps);
                    list.setOnItemClickListener(Result.this);
                    list.setAdapter(adapter);
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... params) {
                String text = params[0];
                String neighborhood = params[1];
                String lat = params[2];
                String lng = params[3];
                String from = params[4];
                String to = params[5];
                return search(text,neighborhood,lat,lng,from,to);
            }
        }
        String text = getIntent().getStringExtra("text");
        Post post = new Post(Result.this);
        post.execute(text,neighborhood,String.valueOf(lat),String.valueOf(lng),"1","30");
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long lng) {

        Intent intent = new Intent(this, Company.class);
        intent.putExtra("logo", bitmaps.get(i));
        intent.putExtra("id",l.get(i).getCompany_id());
        intent.putExtra("pid",l.get(i).getProductId());

        startActivity(intent);
    }
}
