package com.apimenti.fazdelivery;

import android.util.Log;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Toast;

/**
 * Created by joaopaulo on 21/05/13.
 */
public class EndlessScrollListener implements OnScrollListener {
    private int visibleThreshold = 5;
    private int currentPage = 0;
    private int previousTotal = 0;
    private boolean loading = true;

//    private String text;
//    private String neighborhood;
//    private String lat;
//    private String lng;


    public EndlessScrollListener() {
    }

    public EndlessScrollListener(int visibleThreshold) {
        this.visibleThreshold = visibleThreshold;
    }

//    public EndlessScrollListener(String text, String neighborhood, String lat, String lng, String page) {
//        this.text = text;
//        this.neighborhood = neighborhood;
//        this.lat = lat;
//        this.lng = lng;
//        this.currentPage = Integer.valueOf(page);
//    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        if (loading) {
            Log.d("scroll","totalItemCount: "+String.valueOf(totalItemCount));
            Log.d("scroll","previousTotal: "+String.valueOf(previousTotal));
            if (totalItemCount > previousTotal) {
                Log.d("scroll","loading false");
                loading = false;
                previousTotal = totalItemCount;
                currentPage++;
            }
        }
        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            Log.d("scroll", "Scrolled");
            // I load the next page of gigs using a background task,
            // but you can call any function here.
//            new Post().execute(text,neighborhood,lat,lng,String.valueOf(currentPage + 1));
            loading = true;
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }
}
