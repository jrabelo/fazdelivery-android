package com.apimenti.fazdelivery;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.apimenti.fazdelivery.post.Designer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by joaopaulo on 11/06/13.
 */
public class LoadResultImages extends AsyncTask<Serializable,Void,List<Bitmap>> {

    LoadResultImages() {
    }

    @Override
    protected List<Bitmap> doInBackground(Serializable... params) {
        ArrayList<Products> p;
        List<Bitmap> bitmaps = new ArrayList<Bitmap>();
        List<String> urls = new ArrayList<String>();
        try {
            Log.d("post", String.valueOf(params[0]));
            p = new ArrayList<Products>((Collection<? extends Products>) params[0]);

            for (Products product: p) {
                String url = product.getImage_url();
                urls.add(url);
            }
            Log.d("post","entrou3");
            Designer drawing = new Designer();
            drawing.execute(urls);
            bitmaps = drawing.get();
            Log.d("post","entrou4");
            return bitmaps;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Log.d("post","deu algo errado");
        return bitmaps;
    }
}
