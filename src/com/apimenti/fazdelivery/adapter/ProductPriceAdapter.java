package com.apimenti.fazdelivery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.apimenti.fazdelivery.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by joaopaulo on 16/05/13.
 */
public class ProductPriceAdapter extends BaseAdapter {

    private JSONArray productVariations;
    private LayoutInflater mInflater;

    public ProductPriceAdapter(Context context, JSONArray productVariations) {
        mInflater = LayoutInflater.from(context);
        this.productVariations = productVariations;
    }


    static class ViewHolder {
        private TextView name;
        private TextView price;
    }
    @Override
    public int getCount() {
        return productVariations.length();
    }

    @Override
    public JSONObject getItem(int i) {
        try {
            return productVariations.getJSONObject(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.product_price_adapter_item,null);
            holder = new ViewHolder();

            holder.name = (TextView) view.findViewById(R.id.productDetailName);
            holder.price = (TextView) view.findViewById(R.id.productDetailPrice);

            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

        JSONObject p = null;
        try {
            p = productVariations.getJSONObject(i);
            String price = DecimalFormat.getCurrencyInstance().format(Float.valueOf(p.getString("price")));

            holder.name.setText(p.getString("name"));
            holder.price.setText(price);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }
}
