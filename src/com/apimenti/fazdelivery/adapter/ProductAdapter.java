package com.apimenti.fazdelivery.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.apimenti.fazdelivery.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

/**
 * Created by joaopaulo on 11/06/13.
 */
public class ProductAdapter extends BaseAdapter {

    private JSONObject product;
    private LayoutInflater mInflater;
    private JSONArray keys;

    public ProductAdapter(Context context, JSONObject product) {
        mInflater = LayoutInflater.from(context);
        this.product = product;
        this.keys  = product.names();
    }


    static class ViewHolder {
        private TextView name;
        private TextView price;
        public TextView description;
    }
    @Override
    public int getCount() {
        return product.length();
    }

    @Override
    public JSONObject getItem(int i) {
        try {
            return product.getJSONObject(keys.getString(i));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.products_list_adapter_item,null);
            holder = new ViewHolder();

            holder.name = (TextView) view.findViewById(R.id.itemTitle);
            holder.description = (TextView) view.findViewById(R.id.itemDescription);
            holder.price = (TextView) view.findViewById(R.id.itemPrice);

            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

        JSONObject p = null;
        try {
            p = product.getJSONObject(keys.getString(i));
            String price = "R$"+p.getString("price");

            holder.name.setText(p.getString("name"));
            holder.description.setText(p.getString("description"));
            holder.price.setText(price);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }
}
