package com.apimenti.fazdelivery.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.apimenti.fazdelivery.Products;
import com.apimenti.fazdelivery.R;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by joaopaulo on 16/05/13.
 */
public class SearchAdapter extends BaseAdapter {

    private List<Products> products;
    private List<Bitmap> bitmaps;
    private LayoutInflater mInflater;

    public SearchAdapter(Context context, List<Products> products, List<Bitmap> bitmaps) {
        mInflater = LayoutInflater.from(context);
        this.products = products;
        this.bitmaps = bitmaps;

    }


    static class ViewHolder {
        private TextView name;
        private TextView store;
        private TextView price;
        private ImageView img;
    }
    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int i) {
        return products.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.search_adapter_item,null);
			holder = new ViewHolder();

            holder.name = (TextView) view.findViewById(R.id.name);
            holder.price = (TextView) view.findViewById(R.id.price);
            holder.store = (TextView) view.findViewById(R.id.store);
            holder.img = (ImageView) view.findViewById(R.id.searchimg);

            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

        Products p = products.get(i);
        String price = DecimalFormat.getCurrencyInstance().format(Float.valueOf(p.getPrice()));

        holder.name.setText(p.getName());
        holder.price.setText(price);
        holder.store.setText(p.getCompany_name());
        holder.img.setImageBitmap(bitmaps.get(i));
        return view;
    }
}
